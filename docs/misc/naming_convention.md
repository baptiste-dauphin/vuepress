# Goal
Create a document to describe naming conventions for your company machines. 

(Cf. https://en.wikipedia.org/wiki/Naming_convention)

## Server naming
### Proposal 1

`Environment`-`Zone-Technology`-`Identifiant`-`Indice`

- Environment : prod, prelive, staging, dev, poc
- Zone: par, stras, ny, bdx, ...
- Technologies: mysql, nginx, haproxy, ...
- Identifiant: api, streaming, maps ...
- Indice: Number in [01;99]

### Proposal 2 
To be updated

### Other proposal
- Singular names
- Always lowercase names
- No product name in orchestrators cluster hostnames
- Maximum of 2 elements after zone element
- Environment element always in first position
- Rôle element always in third position


## Infra data management
### Gestion des secrets
(user, private key, certificats...)

### Vault
Le but est de __s'abstraire__ au maximum de la __technologie__ et de raisoner en terme de __fonction__.
Yaml view


`To be updated...` just a draft of reflexion


## Vault Path
### Database
Proposal :

```
/env/$env/app/$app/$replication_type/$system_instance_name/db/$db_name/$user_name
```

```bash
/env/staging/app/postgres/cluster/monitoring/system/sst_user
/env/staging/app/postgres/cluster/monitoring/system/clustercheckuser
/env/staging/app/postgres/cluster/monitoring/system/root

/env/staging/app/postgres/cluster/monitoring/db/zabbix/zabbix
/env/staging/app/postgres/cluster/monitoring/db/zabbix/readonly
/env/staging/app/postgres/cluster/monitoring/db/zabbix/readwrite

/env/staging/app/postgres/cluster/monitoring/db/telegraf/telegraf
/env/staging/app/postgres/cluster/monitoring/db/telegraf/readonly
/env/staging/app/postgres/cluster/monitoring/db/telegraf/readwrite

/env/development/app/mysql/standalone/legacy/db/streaming/toto

/env/staging/app/mysql/cluster/api/db/api/toto

/env/production/app/mysql/standalone/server_funct/db/users/toto

/env/development/app/postgres/standalone/wordpress/system/root

/env/staging/app/postgres/cluster/global/system/sst_user
```


Not finished Proposal...

```yaml
service:
	- production:
		- database:
			- user:
				- admin
				- reader
				- api_user
			- blacklist:
			- trends:
		- cache:
	- staging
		- database
		- cache
	- dev
		- database
		- cache
infra:
	- production
		- os
			- root
			- zabbix
	- staging
	- dev
```