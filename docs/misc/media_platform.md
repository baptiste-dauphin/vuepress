# Media / Platform

## Online tool
Name | Description
-|-
[securityheaders](https://securityheaders.com/?q=share.baptiste-dauphin.com&followRedirects=on) | Reporting for CSP and other Security Headers!
[SSL Labs Server test](https://www.ssllabs.com/ssltest/analyze.html?d=share.baptiste-dauphin.com) | Inspect the configuration of any public SSL web server.
[SSL Labs Server client](https://clienttest.ssllabs.com:8443/ssltest/viewMyClient.html) | Shows the TLS capabilities and vulnerabilties of your browser.  
[Regex101](https://regex101.com/) | Regular expression tester
[crt.sh](https://crt.sh/) | Certificates Knowledge base
[Security Trails](https://securitytrails.com/) | DNS knowledge base
[dmarc analyzer](https://www.dmarcanalyzer.com/dmarc/dmarc-record-check/) | check DMARC record, test it and verify whether it’s valid or not.
[Dns Dumpster](https://dnsdumpster.com/#hostanchor) | dns recon & research, find & lookup dns records



## Network tool
- [Gitoyen - BGP Looking glass](https://lg.gitoyen.net/summary/whiskey+vodka+x-ray/ipv4)
- [Cogent - BGP Looking glass](https://www.cogentco.com/en/network/looking-glass)

## Vulnerabilities tracking
[F-Secure Labs](https://labs.f-secure.com/advisories/)

## Test / Automation
Name | Description | Logo
-|-|-
__[GitHub](https://github.com/)__ | Biggest __code hosting__ platform (Owned by Microsoft) | <img :src="$withBase('/src/logo/Github.svg')" alt="git_cheat"> 
__[GitLab](https://about.gitlab.com/devops-tools/)__ | __Code hosting__ + entire DevOps lifecycle. (almost as big as github) | <img :src="$withBase('/src/logo/gitlab-logo-gray-stacked-rgb.svg')" alt="gitlab-logo-gray-stacked-rgb">
[LGTM](https://lgtm.com/#explore) | Continuous __security analysis__. Code best practise and optimization proposal (Pluggable with github) | <img :src="$withBase('/src/logo/lgtm.png')" alt="git_cheat">
[Code Climate](https://codeclimate.com/blog/) | Automated __Code Review__. Technical Debt calculator + estimator working time (Only pluggable with github) | 
[Reproducible Builds](https://reproducible-builds.org/tools/) | Test __independence__ of package. Rebuild a given package with a few variations added and __compares__ it with pre-compiled binary (from large distributed repositories).
[Codacy](https://www.codacy.com/product#integrations) | Third part CI (GitHub, GitLab, Git, BitBucket, Jira, Slack)

## Social
Name | Description | Logo
-|-|-
[Gitter](https://gitter.im/) | Comunity conversation for software developpers. (Authentication only Gitlab / Github / twitter() | 
[Signal](https://signal.org/fr/) | End-to-End messaging app. Used by Edward Snowden | 
[Keybase](https://keybase.io/) | End-to-End messaging and file sharing | 

## Fun
Name | Description
-|-
[Spurious correlations](https://www.tylervigen.com/spurious-correlations) | Misleading and random correlations between unrelated events