# Definitions
Name | TLDR meaning | further explanations
-|-|-
TLDR | Too long I didn't read | [:book:](https://en.wikipedia.org/wiki/Wikipedia:Too_long;_didn%27t_read)
CLI / Promt| Command Line Interpreter / Interface en ligne de commande. Different from Graphical mouse clickable | [:book:]()
Shell Linux | CLI of Linux (sh,bash,dash,csh,tcsh,zsh) | [:book:](https://fr.wikipedia.org/wiki/Shell_Unix#Shells)
Java Heap | shared among all Java virtual machine threads. The heap is the runtime data area from which memory for all __class__ instances and __arrays__ is allocated. | [:book:](https://alvinalexander.com/java/java-stack-heap-definitions-memory)
Java Stack | Each Java virtual machine thread has a private Java virtual machine stack holding __local variables__ and partial results, and plays a part in __method invocation__ and __return__ | [:book:](https://alvinalexander.com/java/java-stack-heap-definitions-memory)

