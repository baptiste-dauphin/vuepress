# Organization

### Everybody hate meeting


### Free-spirited

`When you’re operating on the maker’s schedule, meetings are a disaster. A single meeting can blow a whole afternoon, by breaking it into two pieces each too small to do anything hard in.`  

Paul Graham

### No interruption

Everything can wait

### Better decisions

Simply explaining your problem solves it most of time

### Do not re-invent the wheel
Hello Git-issues

- [GitLab issues](https://docs.gitlab.com/ee/user/project/issues/#parts-of-an-issue)
- [GitHub issues](https://guides.github.com/features/issues/)

[Sources](https://blog.alan.com/bien-etre-au-travail/no-meeting-policy)