---
home: true
heroImage: /tux.jpeg
actions:
  - text: Go explore
    link: /common/system.html
    type: primary
---

I work since 2018 as SysOps and I want to share my knowledge with you and the community. That's how internet must work !

<p align="center">
  <img src="https://gitlab.com/baptiste-dauphin/vuepress/badges/master/pipeline.svg">
</p>