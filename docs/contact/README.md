# About
Email : [baptistedauphin@protonmail.com](mailto:baptistedauphin@protonmail.com)  
PGP : `5A04 0187 EDDD D936 B41E 8268 E457 7920 E027 46B3`  

CV : [cv.baptiste-dauphin.com](http://cv.baptiste-dauphin.com/)  
Landing page [www.baptiste-dauphin.com](http://www.baptiste-dauphin.com/)  
Professional network [LinkedIn](https://www.linkedin.com/in/baptiste-dauphin/)  

Code Hosting [GitLab](https://gitlab.com/baptiste-dauphin/)  

# BlogRoll
[Tout et Rien](https://toutetrien.lithio.fr/a-propos)  
[Patch work kernel](https://patchwork.kernel.org/)  
