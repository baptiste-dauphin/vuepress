# Php
## Troubleshooting
### Version
Get version
```bash
php --version
```

### Var dump
(for debuging) :)  
Run the following script. Either by cli or get through a webserver, like for wordpress debuging with Doctor.
```php
<?php var_dump($_SERVER); ?>
```

### Modules
Get available modules
```bash
php -m
```
