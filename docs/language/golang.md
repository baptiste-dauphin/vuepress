# Golang
## Functions
### Printf  

In this example, `fmt.Printf` formats and writes to standard output:
```golang
fmt.Printf("Binary: %b\\%b", 4, 5)
```
```
Binary: 100\101
```

### Sprintf (format without printing)  
Use fmt.Sprintf to format a string without printing it:
```golang
s := fmt.Sprintf("Binary: %b\\%b", 4, 5)
```
No output but the value is : 
```
s == 'Binary: 100\101'
```

[Sources](https://yourbasic.org/golang/fmt-printf-reference-cheat-sheet/#printf)

## Optimization

[Optimizing a golang service to reduce over 40 cpu](https://medium.com/coralogix-engineering/optimizing-a-golang-service-to-reduce-over-40-cpu-366b67c67ef9)

## Templating
https://golangdocs.com/templates-in-golang
