# Network
## Command
### Common packages
for those binaries
```bash
ifconfig, netstat, rarp, route, ip, dig
```

from those packages

```bash
apt install net-tools iproute2 dnsutils
```

### Ip, arp, route
| Command                                                                | meaning                    |
|:-----------------------------------------------------------------------|:---------------------------|
| ip a                                                                   | get IP of the system       |
| ip r                                                                   | get routes of the system   |
| ip route change default via 99.99.99.99 dev ens8 proto dhcp metric 100 | modify default route       |
| ip addr add 88.88.88.88/32 dev ens4                                    | add (failover) IP to a NIC |

```bash
ip route add default via 10.20.30.40 src 88.88.88.88
```

```bash
ip route add default scope global src 88.88.88.88 \
    nexthop via 10.20.30.40 dev ens224 weight 1 \
    nexthop via 10.20.30.41 dev ens224 weight 1
```



### netplan
new ubuntu network manager
```bash
cat /{lib,etc,run}/netplan/*.yaml
```

### Netstat
__Warning__ : Netstat is considered deprecated and not optimized. It's prefered to use `ss` instead

Show network connections, listening process

| command         | specification                               |
|:----------------|:--------------------------------------------|
| netstat -t      | list tcp connections                        |
| netstat -lt     | list listening tcp socket                   |
| netstat -lu     | list listening udp socket                   |
| netstat -ltu    | list listening udp + tcp socket             |
| netstat -lx     | list listening unix socket                  |
| netstat -ltup   | same as above, with info on process         |
| netstat -ltupn  | p(PID), l(LISTEN), t(tcp), n(Convert names) |
| netstat -ltpa   | all = ESTABLISHED (default) LISTEN          |
| netstat -lapute | classic useful usage                        |
| netstat -salope | same                                        |
| netstat -tupac  | same                                        |

### ss
(new quicker way).  
More info on listening process.  
```bash
ss -tlpn
ss -tulipe
ss -lapute
ss -laputen
```

```bash
ss -ltpn sport eq 2377
ss -t '( sport = :ssh )'
ss -ltn sport gt 500
ss -ltn sport le 500
```

## tcpdump
### Tcp
Real time, just see what’s going on, by looking at all interfaces.
`ccze` is for colorized output

```bash
tcpdump -i any -w capturefile.pcap

tcpdump port 80 -w capture_file

tcpdump 'tcp[32:4] = 0x47455420'

tcpdump -n dst host ip

tcpdump -i any -XXXvvv dst host 35.227.35.254
tcpdump -i any -XXXvvv dst host registry.gitlab.com
tcpdump -i any -XXXvvv dst host registry.gitlab.com and port 443

tcpdump -vv -i any port 514

tcpdump -i any -XXXvvv src net 10.0.0.0/8 and dst port 1234 or dst port 4321 | ccze -A

tcpdump -i any port not ssh and port not domain and port not zabbix-agent | ccze -A
```
https://danielmiessler.com/study/tcpdump/

### udp
```bash
tcpdump -i lo udp port 123 -vv -X

tcpdump -vv -x -X -s 1500 -i any 'port 25' | ccze -A
```

https://danielmiessler.com/study/tcpdump/#source-destination

## tcpflow
Print much better payload
```bash
tcpflow -c port 443

tcpflow port 80

tcpflow -i eth0 port 80

tcpflow -c host www.google.com
```

#### List ports a process PID is listening on
```bash
lsof -Pan -p $PID -i
# ss version
ss -l -p -n | grep ",1234,"
```




## systemd-networkd
debian 9 new network management style
```
vim /etc/systemd/network/50-default.network
systemctl status systemd-networkd
systemctl restart systemd-networkd
```

## ENI
old fashioned network management style
## vlan
vlan tagging and route add 
```bash
auto enp61s0f1.3200
iface enp61s0f1.3200 inet static
  address 10.10.10.20/22
  vlan-raw-device enp61s0f1
  post-up ip route add 10.0.0.0/8 via 10.10.10.254

# with package "ifupdown"
auto eth0
    iface eth0 inet static
        address 192.0.2.7/30
        gateway 192.0.2.254
```



## NAT
Activate NAT (Network Address Translation)
```
iptables -t nat -A POSTROUTING -s 10.0.0.0/24 -o eth0 -j MASQUERADE
```

## VPN
### OpenVpn
#### Client
```bash
apt install openvpn resolvconf

sudo openvpn --config /home/baptiste/.openvpn/b_dauphin@vpn.domain.com.ovpn
```
To get `/etc/resolv.conf` automatically managed by your vpn client. You have to add the following lines
```
script-security 2
up /etc/openvpn/update-resolv-conf
down /etc/openvpn/update-resolv-conf
```

## Netcat
Netcat (network catch)
TCP/IP swiss army knife
### Listen
```bash
nc -l 127.0.0.1 -p 80
nc -lvup 514

# listen all ip on tcp port 443
nc -lvtp 443
```
### Check port opening
only for TCP (obviously), UDP is not connected protocol
```bash
nc -znv 10.10.10.10 3306
```

### manually write tcp packet
```
echo '<187>Apr 29 15:26:16 qwarch plop[12458]: baptiste' | nc -u 10.10.10.10 1514
```

## Internet Exchange Point
[FranceIX](https://www.franceix.net/en/technical/france-ix-route-servers/)

## BGP
Bord Gateway Protocol.  
- i-bgp (Internal BGP)
- e-bgp (External BGP)

Internal = Relations in the same AS
External = Relation between various AS

### Best practises

<img :src="$withBase('/src/Influence_des_bonnes_pratiques_sur_les_incidents_BGP_article.pdf')" alt="Influence des bonnes pratiques sur les incidentsBGP">


### Bird
BGP 
Open bird console
```bash
birdc
```

Once inside bird console.  
show routes 
```
show route
```

#### Config example
/etc/bird/bird.conf

Setup the source address for outgoing interface (__krt_prefsrc__)

```
# Config example for bird 1.6 
#debug protocols all;

router id 169.254.2.2;

# Watch interface up/down events
protocol device {
       scan time 10;
}

# Import interface routes (Connected)
# (Not required in this example as kernel import all is used here to workaround the /32 on eth0 GCE VM setup)
#protocol direct {
#       interface "*";
#}

# Sync routes to kernel
protocol kernel {
       learn;
       merge paths on; # For ECMP
       export filter { 
              krt_prefsrc = 10.164.0.6; # Internal IP Address of the strongSwan VM. 
              accept; # Sync all routes to kernel
       };
       import all; # Required due to /32 on GCE VMs for the static route below
}

# Configure a static route to make sure route exists
protocol static {
       # Network connected to eth0
       route 10.164.0.0/20 recursive 10.164.0.1; # Network connected to eth0
       # Or blackhole the aggregate
       # route 10.164.0.0/20 blackhole; 
}

# Prefix lists for routing security
# (Accept /24 as the most specific route)
define GCP_VPC_A_PREFIXES = [ 192.168.0.0/16{16,24} ]; # VPC A address space
define LOCAL_PREFIXES     = [ 10.164.0.0/16{16,24} ];  # Local address space

# Filter received prefixes
filter gcp_vpc_a_in
{
      if (net ~ GCP_VPC_A_PREFIXES) then accept;
      else reject;
}

# Filter advertised prefixes
filter gcp_vpc_a_out
{
      if (net ~ LOCAL_PREFIXES) then accept;
      else reject;
}

template bgp gcp_vpc_a {
       keepalive time 20;
       hold time 60;
       graceful restart aware; # Cloud Router uses GR during maintenance
       #multihop 3; # Required for Dedicated/Partner Interconnect

       import filter gcp_vpc_a_in;
       import limit 10 action warn; # restart | block | disable

       export filter gcp_vpc_a_out;
       export limit 10 action warn; # restart | block | disable
}

protocol bgp gcp_vpc_a_tun1 from gcp_vpc_a
{
       local 169.254.2.2 as 65002;
       neighbor 169.254.2.1 as 65000;
}
```

[Source](https://cloud.google.com/community/tutorials/using-cloud-vpn-with-strongswan?hl=fr)

## NetworkManager
### Fortinet Vpn connexion handling
Instead of install a `dirty vendor client`. You can setup your vpn client by the magnificnet `NetworkManager`.  
You need to install the module.  
Example for `gnome`
```bash
dnf search fortisslvpn                                                                                                                                                                  ─╯
Last metadata expiration check: 7 days, 18:32:25 ago on Mon 28 Dec 2020 02:34:03 PM CET.
================================================================================= Name Matched: fortisslvpn ==================================================================================
NetworkManager-fortisslvpn.x86_64 : NetworkManager VPN plugin for Fortinet compatible SSLVPN
NetworkManager-fortisslvpn-gnome.x86_64 : NetworkManager VPN plugin for SSLVPN - GNOME files
plasma-nm-fortisslvpn.x86_64 : Fortigate SSL VPN support for plasma-nm
```
```bash
dnf install NetworkManager-fortisslvpn-gnome
```

And then you're now able to configure a new vpn connection type in networkmanager `gui`.

#### dns-search
```bash
nmcli connection modify <vpn-settings-name> ipv4.dns-search '<domain>,<domain>,<domain>'
```
Ensure it's take into account
```bash
resolvectl status ppp0
resolvectl status
```