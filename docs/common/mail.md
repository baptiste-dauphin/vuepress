# Mail system

There are three main functions that make up an e-mail system.
* First there is the Mail User Agent (MUA) which is the program a user actually uses to compose and read mails.
* Then there is the Mail Transfer Agent (MTA) that takes care of transferring messages from one computer to another.
* And last there is the Mail Delivery Agent (MDA) that takes care of delivering incoming mail to the user's inbox. 

Function | Name | Tool which do this
-|-|-
Compose and read | MUA (User Agent) | mutt, thunderbird
Transferring | MTA (Transfer Agent) | msmtp,  __exim4__, thunderbird
Delivering incoming mail to user's inbox | MDA (Devliery agent) | __exim4__, thunderbird

__MTA__

It exists two types of MTA (Mail Transfert Agent)

- __Mail server__ : like postfix, or sendmail-server 
- __SMTP client__, which only forward to a __SMTP relay__ : like ssmtp (deprecated since 2013), use __mstmp__ instead, 

Check what is your email sender, by looking at the sym link of `sendmail`
```bash
which sendmail
/usr/sbin/sendmail

ls -l /usr/sbin/sendmail
lrwxrwxrwx 1 root root 5 Jul 15  2014 /usr/sbin/sendmail -> ssmtp
```

In this case, ssmtp in my mail sender

## Client
### MSMTP (client config)
msmtp est un client SMTP très simple et facile à configurer pour l'envoi de courriels.  
Son mode de fonctionnement par défaut consiste à transférer les courriels au serveur SMTP que vous aurez indiqué dans sa configuration. Ce dernier se chargera de distribuer les courriels à leurs destinataires.  
Compatible :
- `sendmail` ou `bsd-mailx`
- Handle TLS
- Multiples account
- Multiple authentication method
- Distribution distribution


Installation
```bash
apt install msmtp msmtp-mta
```
```bash
vim /home/baptiste/.msmtprc
```
```bash
hashtag Valeurs par défaut pour tous les comptes.
defaults
auth           on
tls            on
tls_trust_file /etc/ssl/certs/ca-certificates.crt
logfile        ~/.msmtp.log

hashtag Exemple pour un compte Gmail
account        gmail
host           smtp.gmail.com
port           587
from           username@gmail.com
user           username
password       plain-text-password

hashtag Définir le compte par défaut
account default : gmail
```

## Send an email
### Using bsd-mailx (prefered)
First, ensure you have your client (msmtp) configured `/home/baptiste/.msmtprc`  
Get the binary `mail`
```bash
apt install bsd-mailx
```
```bash
echo "mon message ici" | mail -s "un super sujet ici" baptistedauphin76@gmail.com
```

### Using sendmail
Test email sending
```bash
echo "mon message ici" | sendmail baptistedauphin76@gmail.com
```
You run the command... and, oops: sendmail: Cannot open mailhub:25. The reason for this is that we didn't provide mailhub settings at all. In order to forward messages, you need an SMTP server configured. That's where SSMTP performs really well: you just need to edit its configuration file once, and you are good to go.

### Using NetCat (hardcore method)
Manually write each steps of the SMTP protocol.
```bash
nc smtp.free.fr 25
```

```bash
telnet smtp.free.fr 25
Trying 212.27.48.4...
Connected to smtp.free.fr.
Escape character is '^]'.
220 smtp4-g21.free.fr ESMTP Postfix
HELO test.domain.com
250 smtp4-g21.free.fr
MAIL FROM:<test@domain.com>
250 2.1.0 Ok
RCPT TO:<toto@domain.fr>
250 2.1.5 Ok
DATA
354 End data with <CR><LF>.<CR><LF>
Subject: test message
This is the body of the message!
.
250 2.0.0 Ok: queued as 2D8FD4C80FF
quit
221 2.0.0 Bye
Connection closed by foreign host.
```
