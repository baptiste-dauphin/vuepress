# GitLab

Un des objectif de l'approche DevOps, est d'automatiser/industrialiser toutes actions qui sera amenée à être répétée plusieurs fois. Une approche est donc de supprimer la non valeur ajouté au travail.

<img :src="$withBase('/src/logo/devops_lifecycle.png')" alt="DevOps Lifecycle">

## CI
### Example
Extracted from https://gitlab.com/baptiste-dauphin/doc/-/blob/master/.gitlab-ci.yml
```yaml
image: node:10

before_script:
  - npm install gitbook-cli -g
  - gitbook fetch 3.2.3
  - gitbook install

test:
  stage: test
  script:
    - gitbook build . public
  rules:
    - if: $CI_COMMIT_BRANCH != "master"
      changes:
      - docs/**/*
      - .git*
      - book.json
    
pages:
  stage: deploy
  script:
    - gitbook build . public
  artifacts:
    paths:
      - public
    expire_in: 1 week
  rules:
    - if: $CI_COMMIT_BRANCH == "master"
      changes:
      - docs/**/*
      - .git*
      - book.json
```

### Predefined Variables
https://docs.gitlab.com/ee/ci/variables/predefined_variables.html

### Runner
Get a secret from 
>From a GitLab Runner :
```bash
docker-compose --file docker-compose.yml run --rm vault vault kv get -field="$KEY" path/to/secret
```

## Installation
### Packages
GitLab can be installed in most GNU/Linux distributions and in a number of cloud providers. To get the best experience from GitLab, you need to balance performance, reliability, ease of administration (backups, upgrades and troubleshooting), and cost of hosting.

There are many ways you can install GitLab depending on your platform:

- __Omnibus GitLab__: The official deb/rpm packages that contain a bundle of GitLab and the various components it depends on like PostgreSQL, Redis, Sidekiq, etc. `recommended` Global config : `/etc/gitlab/gitlab.rb`
- __GitLab Helm chart__: The cloud native Helm chart for installing GitLab and all its components on Kubernetes.
- __Docker__: The Omnibus GitLab packages dockerized.
- __Source__: Install GitLab and all its components from scratch.

> If in doubt, choose __Omnibus__: The Omnibus GitLab packages are mature, scalable, support high availability and are __used today on GitLab.com__. The Helm charts are recommended for those who are familiar with Kubernetes.

### Via Omnibus package
1. Go to https://about.gitlab.com/install/#debian
2. Click on the distribution, it will dynamically change `terminal` steps lines.
3. Follow steps

### Manually configuring HTTPS
You can only changes gitlab global settings inside `gitlab.rb` and tell gitlab to restart `gitlab-ctl reconfigure`.  
Gitlab Omnibus __manage__ NGINX, Postgres, Redis __BY ITSELF__  
That's why you __can not__ manage theme directly as usual.

```bash
systemctl status nginx
● nginx.service
   Loaded: not-found (Reason: No such file or directory)
   Active: inactive (dead)
```

By default, Omnibus GitLab does not use HTTPS.  
To enable HTTPS for the domain `gitlab.example.com`.  

1 - Edit the `external_url` in `/etc/gitlab/gitlab.rb`.  
```bash
external_url "https://gitlab.example.com"

nginx['ssl_certificate'] = "/etc/gitlab/ssl/gitlab.example.com.crt"
nginx['ssl_certificate_key'] = "/etc/gitlab/ssl/gitlab.example.com.key"

registry['enable'] = true
registry_external_url 'https://registry.example.com'

registry_nginx['ssl_certificate'] = "/etc/gitlab/ssl/registry.example.com.crt"
registry_nginx['ssl_certificate_key'] = "/etc/gitlab/ssl/registry.example.com.key"
```

2 - Create the `/etc/gitlab/ssl` directory and copy your key and certificate there
```bash
mkdir -p /etc/gitlab/ssl
chmod 755 /etc/gitlab/ssl
cp gitlab.example.com.key gitlab.example.com.crt /etc/gitlab/ssl/
```
Make sure you use the full certificate chain in order to prevent SSL errors when clients connect. The full certificate chain order should consist of the server certificate first, followed by all intermediate certificates.

3 - Restart
Now, reconfigure GitLab:
```bash
gitlab-ctl reconfigure
```

If nothing happends, you can `force reload nginx`
```bash
gitlab-ctl hup nginx
```


### Update the SSL Certificates
If the content of your SSL certificates has been updated, but no configuration changes have been made to `gitlab.rb`, then `gitlab-ctl reconfigure` will not affect NGINX. Instead, run `sudo gitlab-ctl hup nginx` to cause NGINX to reload the existing configuration and new certificates gracefully.
```bash
gitlab-ctl hup nginx
```

[source](https://docs.gitlab.com/omnibus/settings/nginx.html#manually-configuring-https)

## Docker container registry (v2)

### Registry client - index.py (prefered)
One of my friend created a python script to request a registry
[original script](https://gist.github.com/ebuildy/ee9926ac0b1b290ee7ed7b75eafe856f)

### Registry client - reg
[Doc](https://docs.gitlab.com/ee/user/packages/container_registry/index.html#delete-images-using-gitlab-cicd)  

CI example : 
To use this example, change the `IMAGE_TAG` variable to match your needs:

```yaml
stages:
  - build
  - clean

build_image:
  image: docker:19.03.12
  stage: build
  services:
    - docker:19.03.12-dind
  variables:
    IMAGE_TAG: $CI_REGISTRY_IMAGE:$CI_COMMIT_REF_SLUG
  script:
    - docker login -u $CI_REGISTRY_USER -p $CI_REGISTRY_PASSWORD $CI_REGISTRY
    - docker build -t $IMAGE_TAG .
    - docker push $IMAGE_TAG
  only:
    - branches
  except:
    - master

delete_image:
  image: docker:19.03.12
  stage: clean
  services:
    - docker:19.03.12-dind
  variables:
    IMAGE_TAG: $CI_PROJECT_PATH:$CI_COMMIT_REF_SLUG
    REG_SHA256: ade837fc5224acd8c34732bf54a94f579b47851cc6a7fd5899a98386b782e228
    REG_VERSION: 0.16.1
  before_script:
    - apk add --no-cache curl
    - curl --fail --show-error --location "https://github.com/genuinetools/reg/releases/download/v$REG_VERSION/reg-linux-amd64" --output /usr/local/bin/reg
    - echo "$REG_SHA256  /usr/local/bin/reg" | sha256sum -c -
    - chmod a+x /usr/local/bin/reg
  script:
    - /usr/local/bin/reg rm -d --auth-url $CI_REGISTRY -u $CI_REGISTRY_USER -p $CI_REGISTRY_PASSWORD $IMAGE_TAG
  only:
    - branches
  except:
    - master
```

### Installation of reg

- Go to [releases](https://github.com/genuinetools/reg/releases)
- Follow the instruction for your os arch
For me amd64 - linux

```bash
# Export the sha256sum for verification.
export REG_SHA256="ade837fc5224acd8c34732bf54a94f579b47851cc6a7fd5899a98386b782e228"

# Download and check the sha256sum.
sudo curl -fSL "https://github.com/genuinetools/reg/releases/download/v0.16.1/reg-linux-amd64" -o "/usr/local/bin/reg" \
  && echo "${REG_SHA256}  /usr/local/bin/reg" | sha256sum -c - \
  && sudo chmod a+x "/usr/local/bin/reg"

# Run it!
reg -h
```

```bash
docker login --username baptiste registry.example.com

reg ls registry.example.com
```



### Garbage collection
[Official doc](https://docs.gitlab.com/ee/administration/packages/container_registry.html#container-registry-garbage-collection)
`Danger`: By running the built-in garbage collection command, it will cause downtime to the Container Registry. If you run this command on an instance in an environment where one of your other instances is still writing to the Registry storage, referenced manifests will be removed. To avoid that, make sure Registry is set to read-only mode before proceeding.  

Container Registry can use considerable amounts of disk space. To clear up some unused layers, the registry includes a garbage collect command. 

GitLab offers a set of APIs to manipulate the Container Registry and aid the process of removing unused tags. Currently, this is exposed using the API,  ut in the future, these controls will be migrated to the GitLab interface.

Project maintainers can delete Container Registry tags in bulk periodically based on their own criteria, however, this alone does not recycle data, it only unlinks tags from manifests and image blobs. To recycle the Container Registry data in the whole GitLab instance, you can use the built-in command provided by `gitlab-ctl`.

### Understanding the content-addressable layers
Consider the following example, where you first build the image:
```bash
# This builds a image with content of sha256:111111
docker build -t my.registry.com/my.group/my.project:latest .
docker push my.registry.com/my.group/my.project:latest
```
Now, you do overwrite `:latest` with a new version:
```bash
# This builds a image with content of sha256:222222
docker build -t my.registry.com/my.group/my.project:latest .
docker push my.registry.com/my.group/my.project:latest
```
Now, the `:latest` tag points to manifest of `sha256:222222`. However, due to
the architecture of registry, this data is still accessible when pulling the
image `my.registry.com/my.group/my.project@sha256:111111`, even though it is
no longer directly accessible via the `:latest` tag.


### Recycling unused tags
There are a couple of considerations you need to note before running the
built-in command:

- The built-in command will stop the registry before it starts the garbage collection.
- The garbage collect command takes some time to complete, depending on the amount of data that exists.
- If you changed the location of registry configuration file, you will need to specify its path.
- After the garbage collection is done, the registry should start up automatically.

If you did not change the default location of the configuration file, run:
```bash
sudo gitlab-ctl registry-garbage-collect
```
This command will take some time to complete, depending on the amount of
layers you have stored.

You may also remove all unreferenced manifests, although this is a way more destructive operation, and you should first understand the implications.

### Removing unused layers not referenced by manifests
`WARNING` : This tool option `-m` has a lot of bug on gitlab issues, so it's not recommended to run it without snapshot / backup.  

The GitLab Container Registry follows the same default workflow as Docker Distribution: retain all layers, even ones that are unreferenced directly to allow all content to be accessed using context addressable identifiers.

However, in most workflows, you don't care about old layers if they are not directly
referenced by the registry tag. The `registry-garbage-collect` command supports the
`-m` switch to allow you to remove all unreferenced manifests and layers that are
`not directly accessible via tag`:

```bash
sudo gitlab-ctl registry-garbage-collect -m
```

### Running the garbage collection on schedule
Ideally, you want to run the garbage collection of the registry regularly on a weekly basis at a time when the registry is not being in-use. The simplest way is to add a new crontab job that it will run periodically once a week.

Create a file under `/etc/cron.d/registry-garbage-collect`:
```bash
SHELL=/bin/sh
PATH=/usr/local/sbin:/usr/local/bin:/sbin:/bin:/usr/sbin:/usr/bin

# Run every Sunday at 04:05am
5 4 * * 0  root gitlab-ctl registry-garbage-collect
```

### Expiration policy / Cleanup policy
- Renamed from "expiration policy" to "cleanup policy" in GitLab 13.2.
- Enable automatically for project created after GitLab 12.8

[Official doc](https://docs.gitlab.com/ee/user/packages/container_registry/#create-a-cleanup-policy)

Field | Description
-|-
`Cleanup policy` |  Turn the policy on or off.
`Expiration interval` | How long tags are exempt from being deleted.
`Expiration schedule` | How often the policy should run.
`Number of tags to retain` |  How many tags to always keep for each image.
`Tags with names matching this regex pattern expire:` | The regex pattern that determines which tags to remove. For all tags, use .*. See other regex pattern examples.
`Tags with names matching this regex pattern are preserved:` |  The regex pattern that determines which tags to preserve. The latest tag is always preserved. For all tags, use .*. See other regex pattern examples.

You can not change the default cleanup policy. The only thing you can change is after having enabled it to a project.

Here is the default : 

<img :src="$withBase('/src/gitlab_default_cleanup_policy.png')" alt="Gitlab Default Cleanup Policy">

## Upgrade
0. Snapshot your server (for vmware usage or similarly)

1. Make a system backup (Optional)  
*__Warning__* : Can fully consumed your file system  
saved here `/var/opt/gitlab/backups`
```bash
gitlab-rake gitlab:backup:create STRATEGY=copy
```

2. Update GitLab + all system packages
```bash
apt-get update
apt-get upgrade
```

3. Reboot if new linux version
```bash
systemctl reboot
```

4. If misbehaving after update (which happens often) you can force restart gitlab
```bash
gitlab-ctl restart
```

## Control gitlab
```bash
gitlab-ctl status

# Start all GitLab components
gitlab-ctl start

# Stop all GitLab components
gitlab-ctl stop

# Restart all GitLab components
gitlab-ctl restart



# Tail all logs; press Ctrl-C to exit
gitlab-ctl tail

# Drill down to a sub-directory of /var/log/gitlab
gitlab-ctl tail gitlab-rails

# Drill down to an individual file
gitlab-ctl tail nginx/gitlab_error.log
```

[Sources](https://docs.gitlab.com/omnibus/maintenance/)


## Runner
### Advanced config
#### [runners.docker]
[official doc](https://gitlab.com/gitlab-org/gitlab-runner/-/blob/master/docs/configuration/advanced-configuration.md#the-runnersdocker-section)  
override hosts file (`/etc/hosts`)  :
- `extra_hosts`  
- `/etc/gitlab-runner/config.toml`

```toml
[[runners]]
  url = "http://git.example.org/ci"
  token = "TOKEN"
  name = "my_runner"
  executor = "docker"
  [runners.docker]
    host = "tcp://<DOCKER_DAEMON_IP>:2375"
    image = "..."
    ...
    extra_hosts = ["git.example.org:192.168.0.39"]
```

## Object storage
### Migrate from local to remote S3
```bash
gitlab-rake gitlab:lfs:migrate
gitlab-rake gitlab:lfs:migrate_to_local

gitlab-rake gitlab:artifacts:migrate
gitlab-rake gitlab:artifacts:migrate_to_local

gitlab-rake "gitlab:uploads:migrate:all"
gitlab-rake "gitlab:uploads:migrate:all"
```

### Cleanup
Default : `DRY_RUN=true`
```bash
gitlab-rake gitlab:cleanup:orphan_lfs_file_references
gitlab-rake gitlab:cleanup:orphan_lfs_file_references DRY_RUN=false

gitlab-rake gitlab:cleanup:orphan_lfs_files
gitlab-rake gitlab:cleanup:orphan_lfs_files DRY_RUN=false

gitlab-rake gitlab:cleanup:project_uploads
gitlab-rake gitlab:cleanup:project_uploads DRY_RUN=false

gitlab-rake gitlab:cleanup:orphan_job_artifact_files
gitlab-rake gitlab:cleanup:orphan_job_artifact_files DRY_RUN=false

gitlab-rake gitlab:cleanup:orphan_job_artifact_files
gitlab-rake gitlab:cleanup:orphan_job_artifact_files DRY_RUN=false
```

## Integrity check
```bash
gitlab-rake gitlab:check
gitlab-rake gitlab:git:fsck
gitlab-rake gitlab:artifacts:check
gitlab-rake gitlab:lfs:check
gitlab-rake gitlab:uploads:check
gitlab-rake gitlab:ldap:check
gitlab-rake gitlab:list_repos

```

## Pages
### New Pages website from a forked sample
To get started with GitLab Pages from a sample website, the easiest way to do it is by using one of the [bundled templates](https://docs.gitlab.com/ee/user/project/pages/getting_started/pages_bundled_template.html). If you don’t find one that suits your needs, you can opt by forking (copying) a [sample project from the most popular Static Site Generators](https://gitlab.com/pages).

<img :src="$withBase('/src/new_project_for_pages_v12_5.png')" alt="pages flow">

1. Fork a sample project from the GitLab Pages examples group.
2. From the left sidebar, navigate to your project’s CI/CD > Pipelines and click __Run pipeline__ on master branch to trigger GitLab CI/CD to build and deploy your site to the server.
3. Once the pipeline has finished successfully, find the link to visit your website from your project’s Settings > Pages. It can take approximately 30 minutes to be deployed. Wait several minutes for your static files to be push and accessible to `namespace.gitlab.io`. At the start you'll just get 401 or 404 error.
4. (Bonus HTTPS) __Once__ your site is correctly. You can request gitlab make a request to Let's encrypt on your behalf.  It should take approximatively 30 minutes. You'll see a `CN` under your domain. and a link the access your website https. Because if you activate HTTPS just after pipeline finish, the let's encrypt will not work, and gitlab will not be validated by LE. 

<img :src="$withBase('/src/www.baptiste-dauphin.com_cn.png')" alt="common name">

[sources](https://docs.gitlab.com/ee/user/project/pages/getting_started/fork_sample_project.html)

### Life Cycle
[pages life cycle](https://about.gitlab.com/stages-devops-lifecycle/pages/)

## Agile organization
[GitLab for agile software develoment](https://about.gitlab.com/blog/2018/03/05/gitlab-for-agile-software-development/)

## Support version
[Supported OS](https://docs.gitlab.com/omnibus/package-information/deprecated_os.html)

## Troubleshoot
- Check the Registry logs (e.g. `/var/log/gitlab/registry/current`)
- GitLab production logs for errors (e.g. `/var/log/gitlab/gitlab-rails/production.log`). You may be able to find clues there.
- Default values (overrides by `/etc/gitlab/gitlab.rb`) (`/opt/gitlab/etc/gitlab.rb.template`)

### NGINX Logs
For Omnibus installations, NGINX logs reside in:

- `/var/log/gitlab/nginx/gitlab_access.log` contains a log of requests made to GitLab.
- `/var/log/gitlab/nginx/gitlab_error.log` contains a log of NGINX errors for GitLab.
- `/var/log/gitlab/nginx/gitlab_pages_access.log` contains a log of requests made to Pages static sites.
- `/var/log/gitlab/nginx/gitlab_pages_error.log` contains a log of NGINX errors for Pages static sites.
- `/var/log/gitlab/nginx/gitlab_registry_access.log` contains a log of requests made to the Container Registry.
- `/var/log/gitlab/nginx/gitlab_registry_error.log` contains a log of NGINX errors for the Container Registry.
- `/var/log/gitlab/nginx/gitlab_mattermost_access.log` contains a log of requests made to Mattermost.
- `/var/log/gitlab/nginx/gitlab_mattermost_error.log` contains a log of NGINX errors for Mattermost.

[source](https://docs.gitlab.com/ee/administration/logs.html#nginx-logs)

### Pages logs
For Omnibus installations, Pages logs reside in `/var/log/gitlab/gitlab-pages/current`

## GitLab Omnibus official
- `Code` : https://gitlab.com/gitlab-org/omnibus-gitlab
- `Changelog` : https://gitlab.com/gitlab-org/omnibus-gitlab/-/blob/master/CHANGELOG.md
- `Architecture` : https://docs.gitlab.com/ee/development/architecture.html#simplified-component-overview
